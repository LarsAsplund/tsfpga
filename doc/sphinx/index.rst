.. include:: ../../generated/sphinx/index.rst

.. toctree::
    :caption: About
    :hidden:

    license_information
    contributing
    release_notes


.. toctree::
    :caption: Technical documentation
    :hidden:

    simulation
    fpga_build
    formal
    netlist_build
    module_structure
    registers

.. toctree::
    :caption: API reference
    :hidden:

    api_reference/tsfpga
    api_reference/tsfpga.registers
    api_reference/tsfpga.vivado
